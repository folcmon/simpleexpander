<?php
/**
 * @package Simple Expander
 * @version 1.2
 */
/*
Plugin Name: Simple Expander
Plugin URI: http://www.clicklaboratory.com/
Description: This plugin adds shortcode which can make accordion section from provided content
Version: 1.2
Author: Kamil Kosakowski
Author URI: specit.com.pl
*/
define('SIMPLE_EXPANDER__PLUGIN_URL', plugin_dir_url(__FILE__));
define('SIMPLE_EXPANDER__PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once SIMPLE_EXPANDER__PLUGIN_DIR . 'class.SimpleExpanderDataExtractor.php';

function simple_expander_js()
{
    wp_enqueue_style('expanderStyles', SIMPLE_EXPANDER__PLUGIN_URL . 'assests/css/main.css', array(), '1.0');
    wp_enqueue_script('mainExpanderJs', SIMPLE_EXPANDER__PLUGIN_URL . 'assests/js/main.js', array('jquery'), '1.0', true);
}

add_action('wp_enqueue_scripts', 'simple_expander_js');

function simple_expander_shortcode($atts, $content = "")
{
    $extractorObj = new \SimpleExpander\SimpleExpanderDataExtractor($atts, $content, "[break]");

    $headerPart = trim($extractorObj->getHeaderPart());
    $contentPart = trim($extractorObj->getContentPart());

    $expanderAtts = $extractorObj->getDataAttributes();
    $wrapperId = isset($expanderAtts['wrapperid']) ? $expanderAtts['wrapperid'] : '';
    $headerId = isset($expanderAtts['headerid']) ? $expanderAtts['headerid'] : '';
    $contentId = isset($expanderAtts['contentid']) ? $expanderAtts['contentid'] : '';
    $clickOnHeader = isset($expanderAtts['clickonheader']) ? $expanderAtts['clickonheader'] : '';
    $expanderClassInHeader = ($clickOnHeader === 'true')? 'expand-button' : '';

    $wrapperClasses = isset($expanderAtts['wrapperclassess']) ? str_replace(',', ' ', $expanderAtts['wrapperclassess']) : '';
    $headerClasses = isset($expanderAtts['headerclassess']) ? str_replace(',', ' ', $expanderAtts['headerclassess']) : '';
    $contentClasses = isset($expanderAtts['contentclassess']) ? str_replace(',', ' ', $expanderAtts['contentclassess']) : '';

    $output = <<<string
    <div id="$wrapperId" class="$wrapperClasses expander-wrapper">
        <div id="$headerId" class="$headerClasses expander-header $expanderClassInHeader">
            $headerPart
        </div>
        <div id="$contentId" class="$contentClasses expander-content">
            $contentPart
        </div>
    </div>
string;

    return $output;
}

add_shortcode('SimpleExpander', 'simple_expander_shortcode');