<?php
/**
 * Created by PhpStorm.
 * User: polcode
 * Date: 17/02/2016
 * Time: 09:35
 */

namespace SimpleExpander;


class SimpleExpanderDataExtractor
{
    private $dataAttributes = [];
    private $header = '';
    private $content = '';
    public function __construct($atts =[],$content = '',$contentDelimiter)
    {
        $this->extractAttributes($atts);
        $this->splitContent($content,$contentDelimiter);
    }

    private function extractAttributes($atts){
        if(!empty($atts)){
            foreach ($atts as $key => $value){
                $this->dataAttributes[$key] = $value;
            }
        }
    }
    private function splitContent($content,$contentDelimiter){
        list($this->header,$this->content) = explode($contentDelimiter,$content);
    }

    public function getDataAttributes(){
        return $this->dataAttributes;
    }

    public function getHeaderPart(){
        return do_shortcode($this->header);
    }
    public function getContentPart(){
        return do_shortcode($this->content);
    }
}