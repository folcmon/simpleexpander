(function($){
    $(document).ready(function(){
        $expanderContentItems = $('.expander-content');
        $expanderContentItems.each(function(){
            $(this).hide();
        });
        $(document).on('click','.expander-header a',function(){
            window.location = $(this).attr('href');
            return false;
        })
        $(document).on('click','.expand-button',function(){
            $(this).closest('.expander-wrapper').find('.expander-content').first().toggle(300);
        });
    });

})(jQuery);